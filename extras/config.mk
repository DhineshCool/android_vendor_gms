# Recorder
PRODUCT_PACKAGES += \
    arcore \
    com.google.android.apps.dialer.call_recording_audio.features \
    quick_tap \
    RecorderPrebuilt

# Pixel Dependencies
ifeq ($(TARGET_SUPPORTS_PIXEL_DEP),true)
PRODUCT_PACKAGES += \
    PixelDependencies
endif
